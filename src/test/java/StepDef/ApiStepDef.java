package StepDef;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.opencsv.CSVWriter;

import ReusbaleComponents.API_reusableComponents;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;

public class ApiStepDef  extends API_reusableComponents
{
	private String newRecord = null;
	List<String[]> data;
	CSVWriter writer = null;
	File file = new File("resources/outPut/FTPRResult.csv"); 
	FileWriter outputfile;
	
//	 ApiStepDef() throws IOException
//	 {     FileWriter outputfile = new FileWriter(file);
//	    writer = new CSVWriter(outputfile, ',', 
//                                      CSVWriter.NO_QUOTE_CHARACTER, 
//                                      CSVWriter.DEFAULT_ESCAPE_CHARACTER, 
//                                      CSVWriter.DEFAULT_LINE_END); 
//	 }
	 
	private static final Logger logger = Logger.getLogger(ApiStepDef.class);

	
	@Given("^\"(.*?)\" makes a get call to \"(.*?)\" with below attributes$")
	public void makes_a_get_call_to_with_below_attributes(String userType, String endpoint, DataTable attributesTable) 
	{

		String restURI = apiEndpointIP + loadProps.getEndpointProProperty(endpoint);
		restURI = prepareRestURIPathParam(restURI, attributesTable, newRecord);
		logger.info("GET Endpoint: " + restURI);
		System.out.println("complete URL -> " + restURI);
		
		initiateRestAPICall(restURI);
		System.out.println(response.asString());
	}
	
	@Then("^verify \"(.*?)\" recieves status code as 200$")
	public void verify_recieves_status_code_as(String arg1 ) throws Throwable 
	{
			boolean flag = validateStatusCode(200);
			logger.debug("Status Code FLAG::"+flag);
			assertTrue(flag);
	}

	@Then("^Validate that the requried attribute value are not Null$")
	public void validate_that_the_requried_attribute_value_are_not_Null(DataTable arg1) throws Throwable 
	{
		
	}
	
	@Given("^\"(.*?)\" makes a get call to \"(.*?)\" with the attributes \"(.*?)\" with \"(.*?)\" and \"(.*?)\"$")
	public void makes_a_get_call_to_with_the_attributes_with_and(String userType, String endpoint, String number, String keytype, String key) throws Throwable 
	{
		
		
		String restURI = apiEndpointIP + loadProps.getEndpointProProperty(endpoint)+"?input="+number+"&inputtype="+keytype+"&key="+key;
		initiateRestAPICall(restURI);
		System.out.println(response.asString());

		
	}
	
	
	@Given("^\"(.*?)\" makes a get call to \"(.*?)\" with the attributes keys \"(.*?)\" with \"(.*?)\" and \"(.*?)\"$")
	public void makes_a_get_call_to_with_the_attributes_withKeys_and(String userType, String endpoint, String number, String keytype, String key) throws Throwable 
	{
		
		number=loadProps.getTestData(number);
		keytype=loadProps.getTestData(keytype);
		key=loadProps.getTestData(key);
		
		
		
		String restURI = apiEndpointIP + loadProps.getEndpointProProperty(endpoint)+"?input="+number+"&inputtype="+keytype+"&key="+key;
		initiateRestAPICall(restURI);
		System.out.println(response.asString());

		
	}

 
}
