package StepDef;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import cucumber.api.java.en.Given; 


public class ServerHealthStepDef {

	private static int port = 4723;
	
	@Given("^User triger the server health check$")
	public void heathCheck() 
	{
		checkIfServerIsRunnning(port);
	}
	
	@Given("^close the notifier$")
	public void closeNotifier() 
	{
	}

	public void email() throws UnknownHostException {

		System.out.println("#########Test Started##########");
		Email email = new SimpleEmail();
		InetAddress myIP=InetAddress.getLocalHost();
		try {
			email.setHostName("smtp.gmail.com");
			email.setSmtpPort(465);
			email.setAuthenticator(new DefaultAuthenticator("thecloudgeeks4@gmail.com", "Yellow*123"));
			email.setSSLOnConnect(true);
			email.setFrom("thecloudgeeks4@gmail.com");
			email.setSubject("SERVER DOWN");
			email.setMsg("Server is Down for IP:"+ myIP.getHostAddress());
			email.addTo("nowalvikash@gmail.com");	
			email.addTo("007hitain@gmail.com");
			email.addTo("shashanksharma.er104@gmail.com");
			email.addTo("mahakgarg1223@gmail.com");
			email.send();

			System.out.println("#########Email Sent##########");
		} catch (EmailException e) {
			e.printStackTrace();
		}
	}
	
	public boolean checkIfServerIsRunnning(int port) {

		boolean isServerRunning = true;
		ServerSocket serverSocket;
		while (isServerRunning == true) {
			try {
				serverSocket = new ServerSocket(port);
				serverSocket.close();
				isServerRunning = false;
				System.out.println("Server is not Running");
				
				if (isServerRunning==false) {
					email();
				}
				
			} catch (IOException e) {
				// If control comes here, then it means that the port is in use
				System.out.println("Server is Running");
				isServerRunning = true;
			} finally {
				serverSocket = null;
			}
		}
		return isServerRunning;
	}

}
