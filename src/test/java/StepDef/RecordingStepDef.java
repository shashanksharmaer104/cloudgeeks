package StepDef;

import static org.monte.media.FormatKeys.EncodingKey;
import static org.monte.media.FormatKeys.FrameRateKey;
import static org.monte.media.FormatKeys.KeyFrameIntervalKey;
import static org.monte.media.FormatKeys.MIME_AVI;
import static org.monte.media.FormatKeys.MediaTypeKey;
import static org.monte.media.FormatKeys.MimeTypeKey;
import static org.monte.media.VideoFormatKeys.CompressorNameKey;
import static org.monte.media.VideoFormatKeys.DepthKey;
import static org.monte.media.VideoFormatKeys.ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE;
import static org.monte.media.VideoFormatKeys.QualityKey;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;

import org.monte.media.Format;
import org.monte.media.FormatKeys.MediaType;
import org.monte.media.math.Rational;
import org.monte.screenrecorder.ScreenRecorder;

import ReusbaleComponents.MyScreenRecorder;
import cucumber.api.java.en.Given;

public class RecordingStepDef {

	@Given("^User start the recording$")
	public void startCapturing() 
	{
		try {
			MyScreenRecorder.startRecording("Recording");
			System.out.println("#################Recording is Started##############");
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	
	@Given("^User stop the recording$")
	public void stopCapturing() 
	{
		try {
			MyScreenRecorder.stopRecording();
			System.out.println("#################Recording is Stopped##############");
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	
}
