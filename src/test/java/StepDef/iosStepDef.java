package StepDef;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.When;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class iosStepDef 
{
	private Scenario scenario;

	private static final String MobilecapabilitiesabilityType = null;
	AppiumDriver driver;
	DesiredCapabilities capabilities = new DesiredCapabilities();
	private static final int port = 4723;

	@Before
	public void beforeSteps(Scenario scenario) 
	{
		this.scenario = scenario;
	}
	
	@When("^\"(.*?)\" sets the prerequisite setting to launch the Andrid app$")
	public void sets_the_prerequisite_setting_to_launch_the_Andrid_app(String arg1) throws Throwable 
	{
		
		System.out.println("Starting the set  the  capabilitiesabiliteis ");
		
		capabilities.setCapability("deviceName", "ZF62222XJM");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("appPackage", "com.bankservices");
		capabilities.setCapability("appActivity", ".main.LoginActivity");
		capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "60");
		
		System.out.println("Starting the Driver instance");

		
		driver= new AndroidDriver<MobileElement>(new URL("http://192.168.225.173:4723/wd/hub"), capabilities);
		
		System.out.println("Starting writing the scripts");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//android.widget.ImageView[contains(@resource-id,'imageView_facebook')]")).click();
		Thread.sleep(5000);

		Thread.sleep(5000);
		driver.findElement(By.xpath("//android.widget.EditText[contains(@resource-id,'m_login_email')]")).sendKeys("sapeint");
		driver.findElement(By.xpath("//android.widget.EditText[contains(@resource-id,'m_login_password')]")).sendKeys("sapeint");
		driver.findElement(By.xpath("//android.widget.Button[contains(@text,'Log In')]")).click();
		
		//String s1 = driver.findElement(By.xpath("//android.widget.EditText[contains(@resource-id,'login_error')]")).getText();


		Thread.sleep(5000);
		
		//System.out.println(s1);
		System.out.println("Script has been completed");

	
	}

	@When("^\"(.*?)\" sets the prerequisite setting to launch the iOS app$")
	public void sets_the_prerequisite_setting_to_launch_the_iOS_app(String arg1) throws Throwable 
	{
		
		
		
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
		capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "13.2");
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 8 Plus");
		capabilities.setCapability(MobileCapabilityType.APP, "/Users/hitpawar/Library/Developer/Xcode/DerivedData/TestApp-hjevzckddkfwdwhldupglvuysesy/Build/Products/Debug-iphonesimulator/TestApp.app");
		capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");

		System.out.println("Driver is being launched");
		
		URL url = new URL("http://0.0.0.0:4723/wd/hub");
		driver = new AppiumDriver(url, capabilities);
		
		System.out.println("Driver has been launched");
		
		Thread.sleep(5000);

		
		driver.findElement(By.xpath("//XCUIElementTypeTextField[@name='IntegerA']")).sendKeys("5000");
		driver.findElement(By.xpath("//XCUIElementTypeTextField[@name='IntegerB']")).sendKeys("6000");
		driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='Compute Sum']")).click();

		String s1=driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='Answer']")).getText();

		System.out.println(s1);
		System.out.println("Script has been completed");
	
	}

	@When("^\"(.*?)\" sets the prerequisite setting to launch the Andriod Mobile View$")
	public void sets_the_prerequisite_setting_to_launch_the_Andriod_Mobile_View(String arg1) throws Throwable 
	{
		System.out.println("Starting writing the scripts");
		capabilities.setCapability("deviceName", "emulator-5554");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("browserName", "Chrome");
		capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "60");


		System.out.println("Driver is being launched");
		
		URL url = new URL("http://0.00.0:4723/wd/hub");
		driver = new AppiumDriver(url, capabilities);
		
		System.out.println("Driver has been launched");
		Thread.sleep(5000);
		Thread.sleep(5000);
		Thread.sleep(5000);
		Thread.sleep(5000);

		driver.get("https://www.medtronic.com/in-en/index.html");
		driver.findElement(By.xpath("//span[contains(text(),'Search')]")).click();
		driver.findElement(By.xpath("//*[@class='magic-box-input']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@class='magic-box-input']//input")).sendKeys("Jobs");
		Thread.sleep(1000);
		Thread.sleep(4000);
		driver.findElement(By.xpath("//*[@class='coveo-search-button']")).click();
		Thread.sleep(4000);
		Thread.sleep(4000);
		//Thread.sleep(5000);
		//Thread.sleep(5000);
//		driver.findElement(By.xpath("//*[@class='menu-icon']")).click();
//		Thread.sleep(1000);
//
//		driver.findElement(By.xpath("(//*[contains(text(),'Close')])[2]")).click();

		//System.out.println(s1);
		System.out.println("Script has been completed");
	}

	@When("^\"(.*?)\" sets the prerequisite setting to launch the iOS Mobile View$")
	public void sets_the_prerequisite_setting_to_launch_the_iOS_Mobile_View(String arg1) throws Throwable 
	{
	
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
		capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "13.2");
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 8 Plus");
		capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "Safari");
		capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");

		System.out.println("Driver is being launched");
		
		URL url = new URL("http://0.00.0:4723/wd/hub");
		driver = new AppiumDriver(url, capabilities);
		
		System.out.println("Driver has been launched");
		Thread.sleep(5000);
		Thread.sleep(5000);
		Thread.sleep(5000);
		Thread.sleep(5000);

		driver.get("https://www.medtronic.com/in-en/index.html");
		driver.findElement(By.xpath("//span[contains(text(),'Search')]")).click();
		driver.findElement(By.xpath("//*[@class='magic-box-input']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@class='magic-box-input']//input")).sendKeys("Jobs");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@class='coveo-search-button']")).click();
		Thread.sleep(4000);
		Thread.sleep(4000);
//		Thread.sleep(5000);
//		Thread.sleep(5000);
//		//String s1=driver.findElement(By.xpath("//*[@class='CoveoQuerySummary']")).getText();
//		driver.findElement(By.xpath("//*[@class='menu-icon']")).click();
//		Thread.sleep(1000);
//
//		driver.findElement(By.xpath("(//*[contains(text(),'Close')])[2]")).click();

		//System.out.println(s1);
		System.out.println("Script has been completed");
	}

	
	@When("^\"(.*?)\" sets the prerequisite setting to launch the iOS Mobile View in Cloud$")
	public void sets_the_prerequisite_setting_to_launch_the_iOS_MobileView(String arg1) throws Throwable 
	{
		final String USERNAME = "hitendrapawar1";
		final String AUTOMATE_KEY = "sv72c1u1mUtWsuVVM4af";
		final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

		 
		DesiredCapabilities caps = new DesiredCapabilities();
	    caps.setCapability("browserName", "iPhone");
	    caps.setCapability("device", "iPhone 11");
	    caps.setCapability("realMobile", "true");
	    caps.setCapability("os_version", "13");
	    caps.setCapability("name", "Bstack-[Java] Sample Test");
	    
	    WebDriver driver = new RemoteWebDriver(new URL(URL), caps);
		System.out.println("Driver has been launched");
		Thread.sleep(5000);
		Thread.sleep(5000);
		Thread.sleep(5000);
		Thread.sleep(5000);

		driver.get("https://www.medtronic.com/in-en/index.html");
		driver.findElement(By.xpath("//span[contains(text(),'Search')]")).click();
		driver.findElement(By.xpath("//*[@class='magic-box-input']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@class='magic-box-input']//input")).sendKeys("Jobs");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@class='coveo-search-button']")).click();
		Thread.sleep(4000);
		Thread.sleep(4000);
//		Thread.sleep(5000);
//		Thread.sleep(5000);
//		//String s1=driver.findElement(By.xpath("//*[@class='CoveoQuerySummary']")).getText();
//		driver.findElement(By.xpath("//*[@class='menu-icon']")).click();
//		Thread.sleep(1000);
//
//		driver.findElement(By.xpath("(//*[contains(text(),'Close')])[2]")).click();

		//System.out.println(s1);
		System.out.println("Script has been completed");
	}

	
	@When("^\"(.*?)\" sets the prerequisite setting to launch the app$")
	public void sets_the_prerequisite_setting_to_launch_the_app(String arg1) throws Throwable 
	{
		
		
		
	}

	@When("^\"(.*?)\" launchs the calculate app in \"(.*?)\"$")
	public void launchs_the_calculate_app_in(String arg1, String arg2) throws Throwable {

	}

	@When("^\"(.*?)\" sends inputs \"(.*?)\" in cart$")
	public void sends_inputs_in_cart(String arg1, String arg2, DataTable arg3) throws Throwable {
	  
	}

	@When("^\"(.*?)\" clicks on \"(.*?)\" in cart$")
	public void clicks_on_in_cart(String arg1, String arg2) throws Throwable 
	{
		
	}

	@When("^\"(.*?)\" verifies the \"(.*?)\" in cart$")
	public void verifies_the_in_cart(String arg1, String arg2) throws Throwable 
	{
	  
	}
	
	@When("^\"(.*?)\" sends inputs \"(.*?)\" in Login screen$")
	public void sends_inputs_in_Login_screen(String arg1, String arg2, DataTable arg3) throws Throwable 
	{
	    
	}

	@When("^\"(.*?)\" clicks on \"(.*?)\" in Loing Screen$")
	public void clicks_on_in_Loing_Screen(String arg1, String arg2) throws Throwable 
	{
	   
	}


	
	@When("^\"(.*?)\" verifies the \"(.*?)\" in Loign Screen$")
	public void verifies_the_in_Loign_Screen(String arg1, String arg2) throws Throwable 
	{

	}

	
	@When("^\"(.*?)\" closes the app$")
	public void closes_the_app(String arg1) throws Throwable 
	{
		//driver.quit();

	}

	@When("^\"(.*?)\" launchs the \"(.*?)\" application in mobile View$")
	public void launchs_the_application_in_mobile_View(String arg1, String arg2) throws Throwable 
	{
	   
	}

	@When("^\"(.*?)\" sends inputs \"(.*?)\" in Search Page$")
	public void sends_inputs_in_Search_Page(String arg1, String arg2, DataTable arg3) throws Throwable {

	}

	@When("^\"(.*?)\" clicks on \"(.*?)\" in Search Page$")
	public void clicks_on_in_Search_Page(String arg1, String arg2) throws Throwable {
	 
	}

	@When("^\"(.*?)\" verifies the \"(.*?)\" in Search Page$")
	public void verifies_the_in_Search_Page(String arg1, String arg2) throws Throwable {

	}
	
	@After
	public void afterSteps() 
	{
		embedScreenshot(scenario);
		driver.quit();
		
	}

	private boolean embedScreenshot(Scenario scenario) {
		boolean flag = false;
		System.out.println(scenario.getStatus() + " :Scenario isFailed::" + scenario.isFailed());
			flag = embedScreenshot1(scenario);
		//}
		return flag;
	}
	
	public  void takeScreenshot() {

		try {
			File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(src, new File("Report/Screenshots/"+ "_"
					+ new SimpleDateFormat("ddMMyyyyHHmmss").format(new Date()) + ".png"));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public  boolean embedScreenshot1(Scenario scenario) {
		boolean flag = false;
		try {
			takeScreenshot();
			scenario.embed(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES), "image/png");
			flag = true;
		} catch (WebDriverException wde) {
		} catch (ClassCastException cce) {
		}
		return flag;
	}
	
}
