package RunnerFile;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)

@CucumberOptions(
		features= {"src/test/resources/features/MobileViewiOS_BrowserStack/"},
		glue= {"StepDef"},
		monochrome=true, 
		format={"pretty","html:target/cucumber-reports/cucumber-html-reports", "json:target/cucumber-reports/cucumber-html-reports/common.json"}
		) 

public class MobileViewiOSRunner 
{

}

