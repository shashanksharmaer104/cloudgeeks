Feature: Start Recording & Stop Recording

  @StartRecording
  Scenario: Verify Start Recording
    Given User start the recording

  @StopRecording
  Scenario: Verify Stop Recording
    Given User stop the recording
