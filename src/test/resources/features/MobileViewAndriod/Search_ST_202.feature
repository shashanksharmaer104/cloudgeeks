@ST_202
Feature: Andriod-MobileView-Retail customer verify mobile app in Andriod Mobile View

  Background: 
    When "Retail User" sets the prerequisite setting to launch the Andriod Mobile View

  @ATM_TC001
  Scenario: Retail User performs Search in application
    Given "Retail User" launchs the "Medtronic" application in mobile View
    When "Retail User" sends inputs "for search a product" in Search Page
      | ElementType     | InputValue |
      | input_SearchBox | Jobs       |
    And "Retail User" clicks on "Search button" in Search Page
    Then "Retail User" verifies the "Results" in Search Page
    And "Retail User" closes the app
